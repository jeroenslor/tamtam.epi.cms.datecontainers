﻿Enabled the creation of auto-generated date folders. If a page marked with the IHasContainer interface is created a year and month folder is generated and the item is moved to the folder.

## Install

```
PM> Install-Package TamTam.EPi.Cms.DateContainers
```

## Usage

Create a pagetype that implements the IDateContainer interface. This pagetype is used for the generated date folders.

```csharp
[ContentType(GUID = "EABD0AAD-77AF-4A55-B1D1-00E42427CEB7",AvailableInEditMode = false)]
public class DateContainerPage : PageData, IDateContainer
{
}        
```

Implement the IHasDateContainer interface for pages that should be placed in date containers when created.

```csharp
[ContentType(GUID = "E5C1EB79-3B58-4F1D-8095-2F3F972F3EE4")]
public class NewsDetailPage : PageData, IHasDateContainer
{
    public IDateContainer ConstructDateContainer(ContentReference parent, string name, DateTime startPublish)
    {
        var dateContainer = ServiceLocator.Current.GetInstance<IContentRepository>().GetDefault<SharedDateContainerPage>(parent);
        dateContainer.Name = name;
        dateContainer.StartPublish = startPublish;
        dateContainer.VisibleInMenu = false;
        return dateContainer;
    }
}
```

Implement the IDateContainerRoot interface for the pagetype that represents the root

```csharp
[ContentType(GUID = "FA29A775-7CB3-4E1D-8C91-AEF61E1417B3")]
[AvailableContentTypes(Availability.Specific,Include = new[] {typeof(DateContainerPage),typeof(NewsDetailPage)})]
public class NewsOverviewPage : PageData, IDateContainerRoot
{
}
```